
export default function TrendingList(props){
    return(
        <a className="trendbox" href={props.url} target="_blank" >
            <div className="trends">
                    <span className="twit-name">{props.name}</span>
                <h6>{props.count} tweets</h6>
            </div>
        </a>
    )
}
