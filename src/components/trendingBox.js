import TrendingList from './trendingList'

export default function TrendingBox(props){
    return(
        <div className='trendingBox-container'>
            <h3 className='title'>
                Trending Map
            </h3>
            <h4 className='title-place'>
                {props.trends[10]} Trends
            </h4>
            <div className="trending-list">
                {props.trends.map((trend,i)=>{
                    return(
                        <TrendingList key={i} name={trend.name} url={trend.url} count={trend.tweet_volume}/>
                    )
                })} 
            </div>
            <div className="bottom-box">
                    <p className="disclamer">
                        Click on a place on the map to show the list of current twitter trends. Created by Dante Camacho                
                    </p>
            </div>
        </div>
    )
}