import { GoogleMap, LoadScript } from '@react-google-maps/api';
import TrendingBox from './trendingBox'
import './Map.css'
import {useState,useEffect} from 'react'
import axios from 'axios'

const APIKEY="AIzaSyAiw72IwQlnUTTWjhcfl9C9xJdbZSU_qKs"

const containerStyle = {
  width: '100vw',
  height: '100vh',
};
 
const center = {
  lat: 19.424, 
  lng: -99.141
};
  const options={
  streetViewControl: false,
  fullscreenControl:false,
  mapTypeControl:false
}

export default function Map() {
  const [trends,setTrends]=useState([])
  
  
  useEffect(() => {
    axios.get(process.API_URL+"trends",{
      headers: {
        init: 'true',
      }
    }).then(getTrends => {
        setTrends(getTrends.data)
    }).catch (e=>console.log(`Unable to fetch data: ${e}`))
  },[]);

  const getCoords=(map)=>
  {
    axios.get(process.API_URL+"trends",{
      headers: {
        find: 'true',
        lat:map.latLng.toJSON().lat,
        long:map.latLng.toJSON().lng
      }
    }).then(getTrends => {
      setTrends(getTrends.data)
    }).catch (e=>console.log(`Unable to fetch data: ${e}`))
  }

  return ( 
    <LoadScript
      googleMapsApiKey={APIKEY}
    >
      <div>
        <GoogleMap
          mapContainerStyle={containerStyle}
          center={center}
          zoom={3}
          options={options}
          onClick={getCoords}
        >
        </GoogleMap>
        <TrendingBox
        trends={trends}
        /> 
      </div>
    </LoadScript>
  )
}
